//
//  UjoySDKManager.h
//  UjoySDK
//
//  Created by Alan on 2020/6/22.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UjoySDKLoginInfoModel.h"

typedef NS_ENUM(NSInteger, UjoyApplePayCallBackStatus) {
    UjoyApplePaySuccess = 0,    //支付成功
    UjoyApplePayFail,           //支付失败
    UjoyApplePayUnknow,         //支付结果查询中
    UjoyApplePayOrderCreateSuccess,    //订单创建成功
    UjoyApplePayOrderCreateFailed,    //订单创建成功
};

typedef NS_ENUM(NSInteger, UjoyLoginCallBackStatus) {
    UjoyLoginSuccess = 0,    //登录成功
    UjoyLoginFail,           //登录失败
    UjoyLoginOutSuccess,     //退出登录成功
};

NS_ASSUME_NONNULL_BEGIN

@interface UjoySDKManager : NSObject


/** loginCallBack
 登录成功时SDK会移除登录界面
 status:
 UjoyLoginSuccess = 0,    //登录成功
 UjoyLoginFail,           //登录失败
 登录成功返回loginInfo有效
 登录失败返回responseData有效
 */
@property(nonatomic, copy)void (^loginCallBack)(UjoyLoginCallBackStatus status, UjoySDKLoginInfoModel *loginInfo, NSDictionary *responseData);
@property(nonatomic, copy)void (^closeViewsCallBack)(void); //SDK关闭所有界面后的回调（一级页面的关闭，如切换登录页面、账号绑定、修改密码等）
@property(nonatomic, copy)void (^logoutCallBack)(void); //退出登录回调

@property(class, nonatomic, copy, readonly)NSString *sdkVersions;//获取SDK版本号

//单例实例化，用于操作对象
+ (instancetype)manager;


#pragma mark- SDK初始化
//*必须实现 且不得在didFinishLaunchingWithOptions之前
+ (void)initConfigWithDeviceId:(NSString *)deviceId andComplete:(void(^) (BOOL isSuccess))callBack;
//带channelTag的初始化方法
+ (void)initConfigWithDeviceId:(NSString *)deviceId andChannelTag:(NSString *)channelTag andComplete:(void(^) (BOOL isSuccess))callBack;

#pragma mark- AppDelegate中需要实现
//*必须实现 需要在初始化之前实现
+ (void)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

//如有三方需要拉起交互等，需要实现
+ (void)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
+ (void)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;
+ (void)applicationDidBecomeActive:(UIApplication *)application;

//推送需要实现
+ (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo;
+ (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;

#pragma mark- 登录的展示和移除,登录成功时SDK会自动移除登录界面
+ (void)quickLoginToVC:(UIViewController *)vc; //快速登录
+ (void)loginOrSwitchToVC:(UIViewController *)vc; //切换登录

+ (void)logoutAccount; //退出登录，并清除本地账号信息

#pragma mark- 其他
+ (void)openWebView:(NSString *)link; //内置webView

#pragma mark- 账号管理界面的展示
+ (void)showAccountBindViewTo:(UIViewController *)vc resultBlock:(void (^)(BOOL isSuccess, NSString *msg))block; //显示绑定页面，页面关闭后进入回调
+ (void)showAccountChangePwdViewTo:(UIViewController *)vc andCloseComplete:(void(^) (void))callBack ; //显示修改密码页面，页面关闭后进入回调

+ (void)showConversation:(UIViewController *)vc withPrefillText:(NSString *)text;//显示客服,prefillText可以带入一些文字信息到聊天输入框显示
+ (void)showFaqs:(UIViewController *)vc; //显示FAQ

#pragma mark- 支付
/*拉起内购支付
 对象参数的  属性列表
 NSString *productId;             //传苹果内购的商品id
 NSString *gameServerId;         //游戏区服id
 NSString *price;                //充值商品价格单位，单位：1/100 美元
 NSString *gameOrderId;          //游戏订单 id
 NSString *gameRoleId;          //游戏角色id
 NSString *extra;（非必填）       //创建订单时填写的扩展信息
 */
+ (void)applePayWithOrderInfo:(NSDictionary *)dic andPayCallback:(void (^)(UjoyApplePayCallBackStatus status, NSString *order_id, NSString *msg))callBack;


#pragma mark- 统计事件
+ (void)UjoyTrackEventWithName:(NSString *)eventName andParams:(NSDictionary *)eventParams;


#pragma mark- 通知、推送
@property(nonatomic, copy)void (^ujoyNotifyHandleBlock)(NSString *title, NSString *body);      //点击推送后的回调
+ (void)subscribeMessageNotificationWithTopic:(NSString *)topicName; //推送主题订阅

#pragma mark- Twitter分享
/*拉起内购支付
 params：
 UIViewController *vc;      //当前控制器
 NSString *link;            //分享内容中的URL链接
 NSString *dsp;             //分享的文本内容
 UIImage *img;              //分享的图片
 NSDictionary *dic;（非必填） //目前传入事件参数
 
 CallBack：
 BOOL isShareSuccess;       //分享成功或者失败

 */
+ (void)shareTwitterFromVC:(UIViewController *)vc link:(NSString *)link dsp:(NSString *)dsp imagePath:(NSString *)imagePath analyticsDic:(NSDictionary *)dic callBackBlcok:(void (^)(BOOL isShareSuccess))block;

@end

NS_ASSUME_NONNULL_END
