//
//  UjoySDKLoginInfoModel.h
//  UjoySDK
//
//  Created by alan on 2020/8/31.
//  Copyright © 2020 alan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UjoySDKLoginInfoModel : NSObject
@property (nonatomic, copy) NSString *unionId;
@property (nonatomic, copy) NSString *openId;
@property (nonatomic, copy) NSString *signature; //签名
@property (nonatomic, copy) NSString *loginTime; //登录时间
@property (nonatomic, copy) NSString *email;     //登陆或者绑定ujoy后的邮箱
@property (nonatomic, assign) int loginWay;      //登录方式  0:ujoy账号登录 1:游客登录 2:Fb登录 3:apple登录 4:google登录 5:naver登录 6:twitter登录 7.继承码登录
@property (nonatomic, copy) NSString *appleId;   //苹果 id
@property (nonatomic, copy) NSString *twitterId; //推特 id
@property (nonatomic, copy) NSString *ujoyId;    //平台 id
@property (nonatomic, copy) NSString *payed;    //累计支付金额
@property (nonatomic, copy) NSString *token;    //访问令牌


//| openId    | String | 玩家在游戏内唯一标识                                         |
//| unionId   | String | 玩家在CP内唯一标识                                           |
//| token     | String | 访问令牌                                                     |
//| email     | String | 登陆或者绑定ujoy后的邮箱                                     |
//| mobile    | String | 绑定的手机号，可在用户中心修改绑定（仅ujoy登陆或者游客绑定ujoy后方可修改改，其他方式不支持）； |
//| deviceId  | String | 初始化时传入的deviceId，设备的唯一标识                       |
//| twitterId | String | twitter登陆后的唯一标识                                      |
//| googleId  | String | google登陆后的唯一标识                                       |
//| signature | String | 请求签名                                                     |
//| time      | Long   | 登陆时间                                                     |

@end

