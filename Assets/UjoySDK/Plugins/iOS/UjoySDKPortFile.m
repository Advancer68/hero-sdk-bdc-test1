//
//  UjoySDKPortFile.m
//  UnityFramework
//
//  Created by alan on 2020/8/13.
//

#import <UjoySDK/UjoySDKManager.h>

#import "UnityAppController.h"

#define Function_Init_Success     "init_success"
#define Function_Init_Fail      "init_fail"
#define Function_Share_Success     "share_success"
#define Function_Share_Fail      "share_fail"
#define Function_Bind_Success     "bind_success"
#define Function_Bind_Fail      "bind_fail"
#define Function_Login_Success     "login_success"
#define Function_Login_Fail      "login_fail"

#pragma mark- 初始化
void initSDK_ios(const char *deviceId){
    
    [UjoySDKManager initConfigWithDeviceId:[NSString stringWithUTF8String:deviceId] andComplete:^(BOOL isSuccess) {
        if(isSuccess) {
             NSLog(@"UjoySDK初始化成功。");
            NSDictionary *params = @{@"function":@"init_success",@"message":@""};
            NSData *data = [NSJSONSerialization dataWithJSONObject:params
                                                           options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                             error:nil];
            if (data == nil) {
                return;
            }
            NSString *string = [[NSString alloc] initWithData:data
                                                     encoding:NSUTF8StringEncoding];
            const char *msgR = [string UTF8String];
            UnitySendMessage([@"UjoySDKGameObject" UTF8String], [@"getInstance" UTF8String], msgR);
            UnitySendMessage([@"UjoySDK_Callback_Object" UTF8String], [@"onJavaCallback" UTF8String], msgR);
         } else {
             NSLog(@"UjoySDK初始化失败。");
             NSDictionary *params = @{@"function":@"init_fail",@"message":@""};
             NSData *data = [NSJSONSerialization dataWithJSONObject:params
                                                            options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                              error:nil];
             if (data == nil) {
                 return;
             }
             NSString *string = [[NSString alloc] initWithData:data
                                                      encoding:NSUTF8StringEncoding];
             const char *msgR = [string UTF8String];
             UnitySendMessage([@"UjoySDKGameObject" UTF8String], [@"getInstance" UTF8String], msgR);
             UnitySendMessage([@"UjoySDK_Callback_Object" UTF8String], [@"onJavaCallback" UTF8String], msgR);
         }
    }];


    
}
#pragma mark- 快速登录
void quickLogin_ios(){
    //安排一个游客登录
    [UjoySDKManager quickLoginToVC:GetAppController().rootViewController];
    [UjoySDKManager manager].loginCallBack = ^(UjoyLoginCallBackStatus status, UjoySDKLoginInfoModel * _Nonnull loginInfo, NSDictionary * _Nonnull responseData) {
        switch (status) {
            case UjoyLoginSuccess:
            {
                
                //登录成功返回登录信息
                NSLog(@"登录成功回调:%@",loginInfo);
                NSDictionary *params = @{@"function":@"login_success",@"message":@{
                                                 @"openId":loginInfo.openId,
                                                 @"unionId":loginInfo.unionId,
                                                 @"email":loginInfo.email,
                                                 @"twitterId":loginInfo.twitterId,
                                                 @"appleId":loginInfo.appleId,
                                                 @"signature":loginInfo.signature,
                                                 @"time":loginInfo.loginTime,
                                                 @"payed":loginInfo.payed,
                                                 @"token":loginInfo.token,
                }};
                
                NSData *data = [NSJSONSerialization dataWithJSONObject:params
                                                               options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                                 error:nil];
                if (data == nil) {
                    return;
                }
                NSString *string = [[NSString alloc] initWithData:data
                                                         encoding:NSUTF8StringEncoding];
                const char *msgR = [string UTF8String];
                UnitySendMessage([@"UjoySDK_Callback_Object" UTF8String], [@"onJavaCallback" UTF8String], msgR);
            }
                break;
            case UjoyLoginFail:
            {
                //登录成功返回报错信息
                NSLog(@"登录失败:%@",responseData);
                NSDictionary *params = @{@"function":@"login_fail",@"message":responseData?responseData:@""};
                NSData *data = [NSJSONSerialization dataWithJSONObject:params
                                                               options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                                 error:nil];
                if (data == nil) {
                    return;
                }
                NSString *string = [[NSString alloc] initWithData:data
                                                         encoding:NSUTF8StringEncoding];
                const char *msgR = [string UTF8String];
                UnitySendMessage([@"UjoySDK_Callback_Object" UTF8String], [@"onJavaCallback" UTF8String], msgR);

            }
                break;
            default:
                break;
        }
    };
}

#pragma mark- 切换账号&&打开账号选择
void loginOrSwitch_ios(){
    [UjoySDKManager loginOrSwitchToVC:GetAppController().rootViewController];
    [UjoySDKManager manager].loginCallBack = ^(UjoyLoginCallBackStatus status, UjoySDKLoginInfoModel * _Nonnull loginInfo, NSDictionary * _Nonnull responseData) {
        switch (status) {
            case UjoyLoginSuccess:
            {
                //登录成功返回登录信息
                NSLog(@"登录成功回调:%@",loginInfo);
                NSDictionary *params = @{@"function":@"login_success",@"message":@{
                                                 @"openId":loginInfo.openId,
                                                 @"unionId":loginInfo.unionId,
                                                 @"email":loginInfo.email,
                                                 @"twitterId":loginInfo.twitterId,
                                                 @"appleId":loginInfo.appleId,
                                                 @"signature":loginInfo.signature,
                                                 @"time":loginInfo.loginTime,
                                                 @"token":loginInfo.token,
                }};
                
                NSData *data = [NSJSONSerialization dataWithJSONObject:params
                                                               options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                                 error:nil];
                if (data == nil) {
                    return;
                }
                NSString *string = [[NSString alloc] initWithData:data
                                                         encoding:NSUTF8StringEncoding];
                const char *msgR = [string UTF8String];
                UnitySendMessage([@"UjoySDK_Callback_Object" UTF8String], [@"onJavaCallback" UTF8String], msgR);
            }
                break;
            case UjoyLoginFail:
            {
                //登录成功返回报错信息
                NSLog(@"登录失败:%@",responseData);
                NSDictionary *params = @{@"function":@"login_fail",@"message":responseData?responseData:@""};
                NSData *data = [NSJSONSerialization dataWithJSONObject:params
                                                               options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                                 error:nil];
                if (data == nil) {
                    return;
                }
                NSString *string = [[NSString alloc] initWithData:data
                                                         encoding:NSUTF8StringEncoding];
                const char *msgR = [string UTF8String];
                UnitySendMessage([@"UjoySDK_Callback_Object" UTF8String], [@"onJavaCallback" UTF8String], msgR);
            }
                break;
            default:
                break;
        }
    };
}
#pragma mark- 退出账号
void logout_ios(){
    [UjoySDKManager logoutAccount];
}

#pragma mark- 订阅推送
void subscribeToTopic_ios(const char *topicId){
    [UjoySDKManager subscribeMessageNotificationWithTopic:[NSString stringWithUTF8String:topicId]];
}

#pragma mark- 内购支付
void applePay_ios(const char *json){
    NSData *jsonData = [[NSString stringWithUTF8String:json] dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    [UjoySDKManager applePayWithOrderInfo:dic andPayCallback:^(UjoyApplePayCallBackStatus status, NSString * _Nonnull order_id, NSString * _Nonnull msg) {
        NSNumber *payStatus = @20;
        switch (status) {
            case UjoyApplePaySuccess:
            {
                //支付成功
                payStatus = @10;
            }
                break;
            case UjoyApplePayFail:
            {
                //支付失败
                payStatus = @1;
                NSLog(@"支付失败,打印信息：%@",msg);
            }
                break;
            case UjoyApplePayOrderCreateSuccess:
            {
                //创建订单成功
                payStatus = @21;
            }
                break;

            default:
                break;
        }
        NSDictionary *params = @{@"function":@"pay_callback",@"message":@{@"orderId":order_id?order_id:@"",
                                                                       @"payStatus":payStatus,
                                                                       @"msg":msg?msg:@"",}};
        NSData *data = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                         error:nil];
        if (data == nil) {
            return;
        }
        NSString *string = [[NSString alloc] initWithData:data
                                                 encoding:NSUTF8StringEncoding];
        const char *msgR = [string UTF8String];
        UnitySendMessage([@"UjoySDK_Callback_Object" UTF8String], [@"onJavaCallback" UTF8String], msgR);
    }];
}

#pragma mark- 拉起账号绑定页面
void accountBind_ios(){
    [UjoySDKManager showAccountBindViewTo:GetAppController().rootViewController resultBlock:^(BOOL isSuccess, NSString * _Nonnull msg) {
        NSDictionary *params = @{@"function":isSuccess?@"bind_success":@"bind_fail",@"message":msg?msg:@""};
        NSData *data = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                         error:nil];
        if (data == nil) {
            return;
        }
        NSString *string = [[NSString alloc] initWithData:data
                                                 encoding:NSUTF8StringEncoding];
        const char *msgR = [string UTF8String];
        UnitySendMessage([@"UjoySDK_Callback_Object" UTF8String], [@"onJavaCallback" UTF8String], msgR);
    }];
}

#pragma mark- 客服对话框
void showConversation_ios(){
    [UjoySDKManager showConversation:GetAppController().rootViewController withPrefillText:@""];
}

#pragma mark- 拉起帮助中心
void showFAQ_ios(){
    [UjoySDKManager showFaqs:GetAppController().rootViewController];;
}


#pragma mark- 拉起修改密码页面
void modifyPwd_ios(){
    [UjoySDKManager showAccountChangePwdViewTo:GetAppController().rootViewController andCloseComplete:^{
        
    }];
}

#pragma mark- twitter分享
void shareToTwitter_ios(const char *imagePath , const char *text){

    [UjoySDKManager shareTwitterFromVC:[UIApplication sharedApplication].keyWindow.rootViewController
                                  link:@"https://www.baidu.com" dsp:@"内容" imagePath:[NSString stringWithUTF8String:(imagePath?imagePath:"")]
                            analyticsDic:@{}
                           callBackBlcok:^(BOOL isShareSuccess) {
        if (isShareSuccess) {
            NSLog(@"分享成功");
            NSDictionary *params = @{@"function":@"share_success",@"message":@""};
            NSData *data = [NSJSONSerialization dataWithJSONObject:params
                                                           options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                             error:nil];
            if (data == nil) {
                return;
            }
            NSString *string = [[NSString alloc] initWithData:data
                                                     encoding:NSUTF8StringEncoding];
            const char *msgR = [string UTF8String];
            UnitySendMessage([@"UjoySDK_Callback_Object" UTF8String], [@"onJavaCallback" UTF8String], msgR);

        } else {
            NSLog(@"分享失败");
            NSDictionary *params = @{@"function":@"share_fail",@"message":@""};
            NSData *data = [NSJSONSerialization dataWithJSONObject:params
                                                           options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                             error:nil];
            if (data == nil) {
                return;
            }
            NSString *string = [[NSString alloc] initWithData:data
                                                     encoding:NSUTF8StringEncoding];
            const char *msgR = [string UTF8String];
            UnitySendMessage([@"UjoySDK_Callback_Object" UTF8String], [@"onJavaCallback" UTF8String], msgR);

        }
      }];
}

#pragma mark- 事件采集
void logEvent_ios(const char *event, const char *json){
    NSData *jsonData = [[NSString stringWithUTF8String:json?json:""] dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    [UjoySDKManager UjoyTrackEventWithName:[NSString stringWithUTF8String:event] andParams:dic?dic:@{}];

}

#pragma mark- 事件采集
void openWebView_ios(const char *url){
    [UjoySDKManager openWebView:[NSString stringWithUTF8String:url]];

}


