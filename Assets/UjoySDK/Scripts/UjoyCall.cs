
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace UjoyGames
{
	public class UjoyCall
	{
			#if UNITY_IOS && !UNITY_EDITOR
			//功能
			[DllImport("__Internal")]
			private static extern void initSDK_ios(string deviceId);//初始化SDK
			[DllImport("__Internal")]
			private static extern void quickLogin_ios();//快速登录
			[DllImport("__Internal")]
			private static extern void loginOrSwitch_ios();//切换账号登录
			[DllImport("__Internal")]
			private static extern void logout_ios();//退出登录
			[DllImport("__Internal")]
			private static extern void applePay_ios(string info);//苹果支付
			[DllImport("__Internal")]
			private static extern void accountBind_ios();//绑定账号
			[DllImport("__Internal")]
			private static extern void subscribeToTopic_ios(string topicId);//订阅推送
			[DllImport("__Internal")]
			private static extern void showConversation_ios();//客服
			[DllImport("__Internal")]
			private static extern void showFAQ_ios();//帮助中心
			[DllImport("__Internal")]
			private static extern void modifyPwd_ios();//个人中心的修改密码
        	[DllImport("__Internal")]
        	private static extern void shareToTwitter_ios(string imagePath, string text);//Twitter分享

        	////事件采集
        	[DllImport("__Internal")]
        	private static extern void logEvent_ios(string key, string json);
        	[DllImport("__Internal")]
        	private static extern void openWebView_ios(string url);
     

			#elif UNITY_ANDROID && !UNITY_EDITOR
			private static AndroidJavaClass jc = null;
			private readonly string JavaClassName = "com.ujoy.games.sdk.unity.UjoyProxy";
			private readonly string JavaClassStaticMethod_InitSDK = "initSDK";
            private readonly string JavaClassStaticMethod_LoginOrSwitch = "switchAccount";
			private readonly string JavaClassStaticMethod_QuickLogin = "quickLogin";
			private readonly string JavaClassStaticMethod_GooglePay = "googlePay";
			private readonly string JavaClassStaticMethod_AccountBind = "accountBind";
			private readonly string JavaClassStaticMethod_Subscribe2Topic = "subscribeToTopic";
			private readonly string JavaClassStaticMethod_ShowConversation = "showConversation";
			private readonly string JavaClassStaticMethod_ShowFAQ = "showFaqs";
			private readonly string JavaClassStaticMethod_ModifyPwd = "modifyPwd";
			private readonly string JavaClassStaticMethod_Share_Twitter = "shareToTwitter";
			private readonly string JavaClassStaticMethod_LogEvent = "logEvent";
			private readonly string JavaClassStaticMethod_OpenWebView = "openWebView";
			#endif

		public UjoyCall()
		{
			UjoySDKGameObject.getInstance().setPolyADCall(this);
			#if UNITY_IOS && !UNITY_EDITOR
			#elif UNITY_ANDROID && !UNITY_EDITOR
			if (jc == null) {
				jc = new AndroidJavaClass (JavaClassName);
			}
			#endif
		}

		// Use this for initialization
		public void initSDK(string deviceId)
		{

			#if UNITY_IOS && !UNITY_EDITOR
				initSDK_ios(deviceId);
			#elif UNITY_ANDROID && !UNITY_EDITOR
				if (jc != null) {
					jc.CallStatic (JavaClassStaticMethod_InitSDK,
						UjoySDKGameObject.GameObject_Callback_Name, 
						UjoySDKGameObject.Java_Callback_Function,
						deviceId,"");
				}
			#endif
		}

		public void addInitCallback(Action<bool, string> call)
		{
			UjoySDKGameObject.getInstance().addInitCallback(call);
		}

		public void switchAccount()
		{
			
			#if UNITY_IOS && !UNITY_EDITOR
				loginOrSwitch_ios();		
			#elif UNITY_ANDROID && !UNITY_EDITOR
				if(jc != null){
					jc.CallStatic (JavaClassStaticMethod_LoginOrSwitch);
				}
			#endif


		}

		public void quickLogin()
		{

			#if UNITY_IOS && !UNITY_EDITOR
				quickLogin_ios();		
			#elif UNITY_ANDROID && !UNITY_EDITOR
				if (jc != null){
					jc.CallStatic (JavaClassStaticMethod_QuickLogin);
				}
			#endif

		}

		public void accountBind()
		{

			#if UNITY_IOS && !UNITY_EDITOR
				accountBind_ios();
			#elif UNITY_ANDROID && !UNITY_EDITOR
				if(jc != null){
					jc.CallStatic (JavaClassStaticMethod_AccountBind);
				}
			#endif

		}

		public void pay(string json)
		{
			#if UNITY_IOS && !UNITY_EDITOR
			    applePay_ios(json);
			#endif
			#if UNITY_ANDROID && !UNITY_EDITOR
			    if(jc != null){
			    	jc.CallStatic (JavaClassStaticMethod_GooglePay,json);
			    }
			#endif
		}

		public void subscribeToTopic(string topicId)
		{
			#if UNITY_IOS && !UNITY_EDITOR
				subscribeToTopic_ios(topicId);
			#elif UNITY_ANDROID && !UNITY_EDITOR
			if(jc != null){
				jc.CallStatic (JavaClassStaticMethod_Subscribe2Topic,topicId);
			}
			#endif
		}

		public void showConversation()
		{

			#if UNITY_IOS && !UNITY_EDITOR
				showConversation_ios();
			#elif UNITY_ANDROID && !UNITY_EDITOR
				if(jc != null){
					jc.CallStatic (JavaClassStaticMethod_ShowConversation);
				}
			#endif

		}

		public void showFaqs()
		{

			#if UNITY_IOS && !UNITY_EDITOR
				showFAQ_ios();
			#elif UNITY_ANDROID && !UNITY_EDITOR
				if(jc != null){
					jc.CallStatic (JavaClassStaticMethod_ShowFAQ);
				}
			#endif

		}

		public void modifyPwd()
		{

			#if UNITY_IOS && !UNITY_EDITOR
				modifyPwd_ios();
			#elif UNITY_ANDROID && !UNITY_EDITOR
				if(jc != null){
					jc.CallStatic (JavaClassStaticMethod_ModifyPwd);
				}
			#endif

		}

		public void shareToTwitter(string imagePath, string text)
		{

			#if UNITY_IOS && !UNITY_EDITOR
				shareToTwitter_ios( imagePath,  text);
			#elif UNITY_ANDROID && !UNITY_EDITOR
				if(jc != null){
					jc.CallStatic (JavaClassStaticMethod_Share_Twitter,imagePath,text);
				}
			#endif

		}

		public void logEvent(string key, string json)
		{

			#if UNITY_IOS && !UNITY_EDITOR
				logEvent_ios(key, json);
			#elif UNITY_ANDROID && !UNITY_EDITOR
				if(jc != null){
					jc.CallStatic (JavaClassStaticMethod_LogEvent,key,json);
				}
			#endif

		}

		public void openWebView(string url){
			#if UNITY_IOS && !UNITY_EDITOR
				openWebView_ios(url);
			#elif UNITY_ANDROID && !UNITY_EDITOR
				if(jc != null){
					jc.CallStatic(JavaClassStaticMethod_OpenWebView,url);
				}
			#endif
		}

	}
}
