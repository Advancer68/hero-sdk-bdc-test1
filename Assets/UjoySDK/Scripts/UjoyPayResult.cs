
namespace UjoyGames 
{
	[System.Serializable]
    public class PayResult  
    {
    	public string msg;
    	public string orderId;
    	public UjoyConstant.UjoyPayStatusEnum payStatus;

    }
}