﻿using System;
namespace UjoyGames
{
	public class UjoySDK : UjoyBaseApi
	{
		public static Action<bool, string> ShareCallback = null;
		public static Action<bool, string> AccountBindCallback = null;
		public static Action<PayResult> PayCallback = null;
		public static Action<string> LoginFailCallback = null;
		public static Action<LoginUser> LoginSuccessCallback = null;
	}

}

