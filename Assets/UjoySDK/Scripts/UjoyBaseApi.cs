using System;

namespace UjoyGames
{
	public class UjoyBaseApi
	{
		
		private static bool _sInited;
		private static UjoyCall polyCall = null;

		///<summary>
		///  SDK初始化
		///  建议在游戏主activity的onCreate中调用该接口。
		///</summary>
		///<param name="deviceId">设备唯一标识,不可为空，且不支持纯数字格式的字符串。建议以下两种方式任选一：1.取设备唯一值，如google的adid（玩家清楚缓存后依然可以找回游客账号）；2.根据设备+时间戳，创建UUID（可变，所以玩家清除缓存，就会丢失游客账号）。</param>
		///<param name="initCallback">初始化回调</param>
		/// <example>
		/// <code>
		/// private void actionForInitCallback (bool isSuccess, string msg) {
		/// 	if(isSuccess){
		/// 		Debug.Log ("unity init success");
		/// 	}else{
		/// 		Debug.Log ("unity init fail");
		/// 	}
		/// }
		/// UjoySDK.initSDK("deviceId",actionForInitCallback);
		/// </code>
		/// </example>
		public static void initSDK (string deviceId,Action<bool,string> initCallback)
		{
			if (_sInited) {
				return ;
			}
			if (polyCall == null) {
				polyCall = new UjoyCall ();
			}
			_sInited = true;
			polyCall.addInitCallback (initCallback);
			polyCall.initSDK (deviceId);
		}

		///<summary>
		///  快速登录
		///</summary>
		/// <example>
		/// <code>
		/// private void actionForLoginSuccess (LoginUser user) {
		///		Debug.Log ("unity login success: "+user.openId);
		/// }
		/// private void actionForLoginFail(string msg){
		///		Debug.Log ("unity login fail: "+msg);
		/// }
		/// private void actionForBind(bool isSuccess, string msg){
        /// 	if(isSuccess){
        ///     	Debug.Log ("unity bind success");
        /// 	}else{
        ///     	Debug.Log ("unity bind fail");
        /// 	}
        /// }
		/// UjoySDK.LoginSuccessCallback = actionForLoginSuccess;
		///	UjoySDK.LoginFailCallback = actionForLoginFail;
		///	UjoySDK.AccountBindCallback = actionForBind;
		/// UjoySDK.accountLogin();
		/// </code>
		/// </example>
		public static void quickLogin(){
			if(polyCall != null){
				polyCall.quickLogin ();
			}
		}

		///<summary>
		///  切换账号
		///</summary>
		public static void switchAccount ()
		{
			if (!_sInited) {
				return ;
			}
			polyCall.switchAccount ();
		}

		///<summary>
		///  账号绑定
		///</summary>
		public static void accountBind(){
			if(polyCall != null){
				polyCall.accountBind();
			}
		}

		///<summary>
		///  支付
		///</summary>
		/// <param name="json">支付订单信息</param>
		/// <example>
		/// <code>
		/// private void actionForPayCallback(PayResult payResult){
		/// 	Debug.Log ("unity payResult "+payResult.payStatus);
		/// 	if(payResult.payStatus == UjoyConstant.UjoyPayStatusEnum.UjoyPayStatusUnknown){
		/// 		Debug.Log ("unity payResult unknow: "+payResult.orderId);
		/// 	}else if(payResult.payStatus == UjoyConstant.UjoyPayStatusEnum.UjoyPayStatusFail){
		/// 		Debug.Log ("unity payResult fail: "+payResult.msg);
		/// 	}else if(payResult.payStatus == UjoyConstant.UjoyPayStatusEnum.UjoyPayStatusOrderCreated){
		/// 		Debug.Log ("unity payResult OrderCreated: "+payResult.orderId);
		/// 	}else if(payResult.payStatus == UjoyConstant.UjoyPayStatusEnum.UjoyPayStatusSuccess){
		/// 		Debug.Log ("unity payResult Success: "+payResult.orderId);
		/// 	}
		/// }
		/// UjoySDK.PayCallback = actionForPayCallback;
		/// Hashtable ht = new Hashtable();
		/// string id = System.Guid.NewGuid().ToString("N");
		/// ht.Add("gameOrderId",id);
		/// ht.Add("productId",1);
		/// ht.Add("extra","extra 数据");
		/// ht.Add("price",100f);
		/// ht.Add("gameServerId",1);
		/// ht.Add("gameRoleId",1);
		/// string json = HashtableToJson(ht);
		/// UjoySDK.pay(json);
		/// </code>
		/// </example>
		public static void pay(string json){
			if(polyCall != null){
				polyCall.pay(json);
			}
		}

		///<summary>
		///  订阅FCM主题消息
		///</summary>
		/// <param name="id">主题Id</param>
		/// <example>
		/// <code>
		/// UjoySDK.subscribeToTopic("UJOY_NOTIFY");
		/// </code>
		/// </example>
		public static void subscribeToTopic(string id){
			if(polyCall != null){
				polyCall.subscribeToTopic (id);
			}
		}

		///<summary>
		///  打开helpshift客服页面(暂不使用)
		///</summary>
		/// <example>
		/// <code>
		/// UjoySDK.showConversation();
		/// </code>
		/// </example>
		public static void showConversation(){
			if(polyCall != null){
				polyCall.showConversation ();
			}
		}

		///<summary>
		///  打开helpshift的faqs页面（暂不使用）
		///</summary>
		public static void showFaqs(){
			if(polyCall != null){
				polyCall.showFaqs ();
			}
		}

		///<summary>
		///  修改密码（非必接）
		///  仅使用Ujoy登陆或者绑定过Ujoy的用户支持修改密码
		///</summary>
		public static void modifyPwd(){
			if(polyCall != null){
				polyCall.modifyPwd ();
			}
		}

		///<summary>
		///  分享图片到Twitter
		///</summary>
		/// <param name="imagePath">分享的图片的绝对路径</param>
		/// <param name="msg">分享的文字</param>
		/// <example>
		/// <code>
		/// private void actionForShare(bool isSuccess, string msg){
	   	/// 	if(isSuccess){
		///			Debug.Log ("unity share success");
		///		else{
		///			Debug.Log ("unity share fail:"+msg);
		///		}
		/// }
		/// UjoySDK.ShareCallback = actionForShare;
		/// UjoySDK.shareToTwitter(imagePath,"分享的内容");
		/// </code>
		/// </example>
		public static void shareToTwitter(string imagePath,string text){
			if(polyCall != null){
				polyCall.shareToTwitter (imagePath,text);
			}
		}

		///<summary>
		///  日志上报
		///  具体key和value，按市场提供的表格传入
		///</summary>
		/// <param name="key">日志上报key</param>
		/// <param name="values">日志上报附加参数，可为空</param>
        public static void logEvent(string key,string json) {
        	if (polyCall == null) {
				polyCall = new UjoyCall ();
			}
			polyCall.logEvent (key,json);
		}

		///<summary>
		///  使用webview加载url
		///</summary>
		///<param name="url">需加载的url</param>
		public static void openWebView(string url){
			if(polyCall != null){
				polyCall.openWebView(url);
			}
		}
	}
}

