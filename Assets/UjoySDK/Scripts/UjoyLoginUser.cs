using System.Collections;
using System;
namespace UjoyGames 
{
    [Serializable]
    public class LoginUser  
    {
        public int mode;
        public string appleId;
        public string openId;
        public string token;
        public string unionId;
        public string email;
        public string twitterId;
        public string googleId;
        public string signature;
        public string payed;
        public Int64 time;
    }
}