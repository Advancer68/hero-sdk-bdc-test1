﻿using System;

namespace UjoyGames
{
	public class UjoyConstant
	{
		public enum UjoyPayStatusEnum {
			UjoyPayStatusUnknown = 20,
			UjoyPayStatusFail = 1,
			UjoyPayStatusOrderCreated = 21,
			UjoyPayStatusSuccess = 10
		}
	}
}

