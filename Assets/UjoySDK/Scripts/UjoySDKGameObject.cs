using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace UjoyGames {

	public class UjoySDKGameObject : MonoBehaviour {

		private readonly string Function_Init_Success = "init_success";
		private readonly string Function_Init_Fail = "init_fail";
		private readonly string Function_Login_Success = "login_success";
		private readonly string Function_Login_Fail = "login_fail";
		private readonly string Function_Share_Success = "share_success";
		private readonly string Function_Share_Fail = "share_fail";
		private readonly string Function_Bind_Success = "bind_success";
		private readonly string Function_Bind_Fail = "bind_fail";
		private readonly string Function_Pay_Callback = "pay_callback";

		
		private static UjoySDKGameObject instance = null;
		public static readonly string GameObject_Callback_Name = "UjoySDK_Callback_Object";
		public static readonly string Java_Callback_Function = "onJavaCallback";

		private UjoyCall adCall;
		 
		public static UjoySDKGameObject getInstance()
		{
			if (instance == null) {
				GameObject polyCallback = new GameObject (GameObject_Callback_Name);
				polyCallback.hideFlags = HideFlags.HideAndDontSave;
				DontDestroyOnLoad (polyCallback);

				instance = polyCallback.AddComponent<UjoySDKGameObject> ();
			}
			return instance;
		}
		
		Action<bool, string> initCallback;
		bool isAppFocus = false;

		List<string> cachedMessages = new List<string> (12);

		void OnGUI()
		{
			//Debug.Log ("===> Game onGUI Call");
		}

		void OnApplicationFocus(bool hasFocus)
		{
			Debug.Log ("===> OnApplicationFocus() hasFocus:" + hasFocus);
			isAppFocus = hasFocus;
		}

		public void setPolyADCall(UjoyCall call) {
			adCall = call;
		}

		public void addInitCallback(Action<bool, string> callback) {
			initCallback = callback;
		}

		public void onJavaCallback(string message) {
            int index = message.IndexOf("function");
            int index1 = message.IndexOf("message");
            string function = message.Substring(index+11,index1-index-14);
            string msg = message.Substring(index1+9,message.Length-index1-10);
            if(msg.StartsWith("\"")){
            	msg = msg.Substring(1,msg.Length-2);
            }
            //Debug.Log("-------onJavaCallback function :" + function + " message:" + msg);

            if (function.Equals (Function_Init_Success)) {
				if (initCallback != null) {
					initCallback(true, msg);
				}else {
					Debug.Log("-------onJavaCallback initCallback == null:"+ initCallback);
				}
			}else if (function.Equals (Function_Init_Fail)) {
				if (initCallback != null) {
					initCallback(false, msg);
				}
			}else if(function.Equals (Function_Login_Success)){
				if(UjoySDK.LoginSuccessCallback != null){
                	LoginUser user = JsonUtility.FromJson<LoginUser>(msg);
                	UjoySDK.LoginSuccessCallback(user);
                }
            }else if(function.Equals (Function_Login_Fail)){
                if(UjoySDK.LoginFailCallback != null){
                	UjoySDK.LoginFailCallback(msg);
                }
            }else if(function.Equals (Function_Bind_Success)){
                if(UjoySDK.AccountBindCallback != null){
                	UjoySDK.AccountBindCallback(true,msg);
                }
            }else if(function.Equals (Function_Bind_Fail)){
                if(UjoySDK.AccountBindCallback != null){
                	UjoySDK.AccountBindCallback(false,msg);
                }
            }else if(function.Equals (Function_Share_Success)){
                if(UjoySDK.ShareCallback != null){
                	UjoySDK.ShareCallback(true,msg);
                }
            }else if(function.Equals (Function_Share_Fail)){
                if(UjoySDK.ShareCallback != null){
                	UjoySDK.ShareCallback(false,msg);
                }
            }else if(function.Equals (Function_Pay_Callback)){
                if(UjoySDK.PayCallback != null){
                	PayResult payResult = JsonUtility.FromJson<PayResult>(msg);
                	UjoySDK.PayCallback(payResult);
                }
            }

		}
	}
}

